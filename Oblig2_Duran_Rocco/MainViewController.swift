//
//  ViewController.swift
//  Oblig2_Duran_Rocco
//
//  Created by SP 24 on 20/5/16.
//  Copyright © 2016 Rocco Duran. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import INTULocationManager

private let reuseIdentifier = "WeatherCell"

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var weekForecastCollectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // OPEN WEATHER MAP - API URL PARAMETERS
    let baseURL = "http://api.openweathermap.org/data/2.5/forecast/daily"
    let latitude = "lat"
    let longitude = "lon"
    let mode = "mode"
    let modeJson = "json"
    let apiKey = "APPID"
    let unit = "units"
    let unitMetric = "metric"
    let unitImperial = "imperial"
    
    var forecast: [DayForecast]?
    
    var loading: Bool = false{
        didSet{
            loadingIndicator.hidden = !loading
            
            cityLabel.hidden = loading
            currentWeatherLabel.hidden = loading
            currentTemperatureLabel.hidden = loading
            weekForecastCollectionView.hidden = loading
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        loading = true
        
        if SettingsManager.useCustomLocation{
            if let storedCustomLocation = SettingsManager.customUserLocation{
                // Get stored selected location
                refreshWeatherData(storedCustomLocation)
            }else{
                print("No stored custom location")
                loading = false
            }
        }else{
            getUserLocation()
        }
    }
    
    func getUserLocation(){
        let locMgr = INTULocationManager.sharedInstance()
        locMgr.requestLocationWithDesiredAccuracy(.Neighborhood, timeout: NSTimeInterval(10)){ (location,accuracy,status) in
            switch status{
            case .Success:
                SettingsManager.currentUserLocation = location.coordinate
                self.refreshWeatherData(location.coordinate)
            default:
                let alertController = UIAlertController(title: "Oops!", message:
                    "Could not fetch your location, you can alternatively set one manually in the settings", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: {_ in self.loading = false }))
                alertController.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default, handler: { _ in
                    self.getUserLocation()
                }))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func refreshWeatherData(coordinate: CLLocationCoordinate2D){
        if let configPath = NSBundle.mainBundle().pathForResource("config", ofType: "plist") {
            
            // Get API KEY from config.plist
            let configs = NSDictionary(contentsOfFile: configPath)
            let apiKeyValue = configs?.valueForKey("OpenWeatherMapAPIKEY") as! String
            
            // Get stored unit setting
            let unitValue = SettingsManager.userTemperatureUnit == .Fahrenheit ? unitImperial : unitMetric
            
            // Current date
            let today = NSDate()
            let calendar = NSCalendar.currentCalendar()
            
            Alamofire.request(.GET, baseURL, parameters: [latitude:coordinate.latitude, longitude:coordinate.longitude, mode:modeJson, apiKey:apiKeyValue, unit:unitValue])
                .responseJSON { response in
                    switch response.result {
                    case .Success:
                        if let value = response.result.value{
                            self.forecast = [DayForecast]()
                            var cityName: String?
                            // Parse JSON
                            let json = JSON(value)
                            cityName = json["city"]["name"].string
                            let days = json["list"]
                            for (index,day):(String, JSON) in days {
                                // Weather
                                let temp = day["temp"]["day"].floatValue
                                // Icon Code
                                let iconCode = day["weather"][0]["id"].intValue
                                // Icon Text
                                let iconText = day["weather"][0]["icon"].stringValue
                                // Date
                                let nextDay = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: Int(index)!, toDate: today, options: NSCalendarOptions(rawValue: 0))
                                if let nextDay = nextDay{
                                    self.forecast!.append(DayForecast(day: nextDay, temperature: temp, iconCode: iconCode, iconString: iconText))
                                }
                            }
                            
                            if let forecast = self.forecast {
                                if forecast.count > 0 {
                                    // Set UI for Today
                                    let today = forecast[0]
                                    self.cityLabel.text = cityName ?? "Unknown"
                                    self.currentWeatherLabel.text = today.iconCharacter
                                    self.currentTemperatureLabel.text = today.formattedTemperature(SettingsManager.userTemperatureUnit)
                                    self.forecast = Array(forecast[1..<forecast.count])
                                    self.weekForecastCollectionView.reloadData()
                                }
                            }
                        }
                        self.loading = false
                    case .Failure(let error):
                        print(error)
                        let alertController = UIAlertController(title: "Oops!", message:
                            "Could not get retrieve the weather data, please check your connection", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: {_ in self.loading = false }))
                        alertController.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default, handler: { _ in
                            self.refreshWeatherData(coordinate)
                        }))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("weekForecastCell", forIndexPath: indexPath) as! WeekForecastViewCell
        if let forecast = forecast{
            let f = forecast[indexPath.row]
            cell.dayLabel.text = f.dayName
            cell.temperatureLabel.text = f.formattedTemperature(SettingsManager.userTemperatureUnit)
            cell.weatherIconLabel.text = f.iconCharacter
        }
        return cell
    }
}