//
//  WeekForecastViewCell.swift
//  Oblig2_Duran_Rocco
//
//  Created by Joaquin Rocco on 26/05/2016.
//  Copyright © 2016 Rocco Duran. All rights reserved.
//

import UIKit

class WeekForecastViewCell: UICollectionViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherIconLabel: UILabel!
        
}
