//
//  File.swift
//  Oblig2_Duran_Rocco
//
//  Created by Joaquin Rocco on 27/05/2016.
//  Copyright © 2016 Rocco Duran. All rights reserved.
//

import Foundation
import INTULocationManager

public class SettingsManager {
    struct Defaults{
        static let isFahrenheit = "isFahrenheit"
        static let useCustomLocation = "useCustomLocation"
        static let customLocation = "manualLocation"
        static let currentLocation = "currentLocation"
    }
    
    static var userTemperatureUnit:TemperatureUnit{
        get{
            return NSUserDefaults.standardUserDefaults().boolForKey(Defaults.isFahrenheit) ? .Fahrenheit : .Celcius
        }
        set{
            let fahrenheitSelected = newValue == .Fahrenheit
            NSUserDefaults.standardUserDefaults().setBool(fahrenheitSelected, forKey: Defaults.isFahrenheit)
        }
    }
    
    static var useCustomLocation:Bool{
        get{
            return NSUserDefaults.standardUserDefaults().boolForKey(Defaults.useCustomLocation)
        }
        set{
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: Defaults.useCustomLocation)
        }
    }
    
    static var currentUserLocation: CLLocationCoordinate2D?{
        set{
            if let coord = newValue{
                storeCoord(coord, key: Defaults.currentLocation)
            }
        }
        get{
            return getCoord(Defaults.currentLocation)
        }
    }
    
    static var customUserLocation: CLLocationCoordinate2D?{
        set{
            if let coord = newValue{
                storeCoord(coord, key: Defaults.customLocation)
            }
        }
        get{
            return getCoord(Defaults.customLocation)
        }
    }
    
    private static func storeCoord(coord: CLLocationCoordinate2D, key: String){
        let lat = coord.latitude
        let lon = coord.longitude
        let dic = ["lat":lat,"lon":lon]
        NSUserDefaults.standardUserDefaults().setObject(dic, forKey: key)
    }
    private static func getCoord(key: String) -> CLLocationCoordinate2D?{
        let dic = NSUserDefaults.standardUserDefaults().objectForKey(key) as? [String:CLLocationDegrees]
        if let dic = dic{
            return CLLocationCoordinate2D(latitude: dic["lat"]!, longitude: dic["lon"]!)
        }else{
            return nil
        }
    }
}

enum TemperatureUnit {
    case Fahrenheit
    case Celcius
}
