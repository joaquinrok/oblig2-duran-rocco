//
//  FutureDayController.swift
//  Oblig2_Duran_Rocco
//
//  Created by SP 24 on 20/5/16.
//  Copyright © 2016 Rocco Duran. All rights reserved.
//

import UIKit

class FutureDayView: UIView {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!

    
            required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            let xibView = NSBundle.mainBundle().loadNibNamed("FutureDayView", owner: self, options: nil)[0] as! UIView
            xibView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
            xibView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            self.addSubview(xibView)
        }
}
