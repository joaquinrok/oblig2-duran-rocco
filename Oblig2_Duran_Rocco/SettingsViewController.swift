//
//  SettingsViewController.swift
//  Oblig2_Duran_Rocco
//
//  Created by SP 24 on 20/5/16.
//  Copyright © 2016 Rocco Duran. All rights reserved.
//

import UIKit
import MapKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var temperatureSegmentedControl: UISegmentedControl!
    @IBOutlet weak var locationSwitch: UISwitch!
    
    var annotation: MKPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userTemperatureUnit = SettingsManager.userTemperatureUnit
        temperatureSegmentedControl.selectedSegmentIndex = (userTemperatureUnit == .Fahrenheit) ? 1 : 0
        
        let useCustomLocation = SettingsManager.useCustomLocation
        mapView.hidden = !useCustomLocation
        locationSwitch.on = !useCustomLocation
        setupMap()
    }
    
    func setupMap(){
        if let customCoordinate = SettingsManager.customUserLocation{
            addAnnotation(customCoordinate)
            mapView.setRegion(MKCoordinateRegionMakeWithDistance(customCoordinate, 1000000, 1000000), animated: true)
        }
    }
    
    @IBAction func goBack(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveSettings(sender: AnyObject) {
        let fahrenheitSelected = temperatureSegmentedControl.selectedSegmentIndex == 1
        SettingsManager.useCustomLocation = !locationSwitch.on
        SettingsManager.userTemperatureUnit = fahrenheitSelected ? .Fahrenheit : .Celcius
        goBack(sender)
    }
    
    @IBAction func toggleMap(sender: UISwitch) {
        let useCustomLocation = !sender.on
        mapView.hidden = !useCustomLocation
        if useCustomLocation{
            setupMap()
        }
    }

    @IBAction func selectCustomLocation(sender: UITapGestureRecognizer) {
        let coord = mapView.convertPoint(sender.locationInView(mapView), toCoordinateFromView: mapView)
        SettingsManager.customUserLocation = coord
        addAnnotation(coord)
    }
    
    func addAnnotation(coordinate:CLLocationCoordinate2D){
        if annotation == nil {
            annotation = MKPointAnnotation()
        }
        annotation?.coordinate = coordinate
        mapView.addAnnotation(annotation!)
    }
}
